import 'package:empresas_app/modules/login/model/user_dto.dart';

abstract class LoginState {}

class LoginInitialState extends LoginState {}

class LoginLoadingState extends LoginState {}

class LoginSuccessState extends LoginState {
  final UserDTO user;

  LoginSuccessState({required this.user});
}

class LoginErrorState extends LoginState {}
