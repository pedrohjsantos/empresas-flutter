import 'company_type_dto.dart';

class CompanyDTO {
  late int id;
  late String name;
  late String description;
  late String? email;
  late String? facebook;
  late String? twitter;
  late String? linkedin;
  late String? phone;
  late String? photoUrl;
  late String? city;
  late String? country;
  late CompanyTypeDTO? companyType;

  CompanyDTO.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['enterprise_name'];
    description = json['description'];
    email = json['email_enterprise'];
    facebook = json['facebook'];
    twitter = json['twitter'];
    linkedin = json['linkedin'];
    phone = json['phone'];
    photoUrl = json['photo'];
    city = json['city'];
    country = json['country'];

    if (json['enterprise_type'] != null) {
      companyType = CompanyTypeDTO.fromJson(json['enterprise_type']);
    }
  }
}
