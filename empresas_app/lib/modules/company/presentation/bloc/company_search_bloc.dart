import 'package:empresas_app/modules/company/presentation/bloc/company_search_event.dart';
import 'package:empresas_app/modules/company/presentation/bloc/company_search_state.dart';
import 'package:empresas_app/modules/company/usecase/search_company_use_case.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CompanySearchBloc extends Bloc<CompanySearchEvent, CompanySearchState> {
  final SearchCompanyUseCase useCase;

  CompanySearchBloc({required this.useCase})
      : super(CompanySearchInitalState()) {
    on<CompanySearchCompanyEvent>(_searchCompany);
  }

  Future _searchCompany(
    CompanySearchCompanyEvent event,
    Emitter<CompanySearchState> emit,
  ) async {
    emit(CompanySearchLoadingState());

    final companies = await useCase.call(searchString: event.searchString);

    companies.fold(
      (exception) => emit(CompanySearchErrorState()),
      (result) {
        if (result.isEmpty) {
          emit(CompanySearchEmptyState());
        } else {
          emit(CompanySearchLoadedState(companies: result));
        }
      },
    );
  }
}
