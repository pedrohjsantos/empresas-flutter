import 'package:empresas_app/app/environment.dart';
import 'package:empresas_app/modules/company/datasource/company_data_source.dart';
import 'package:empresas_app/modules/company/presentation/bloc/company_search_bloc.dart';
import 'package:empresas_app/modules/company/repository/company_search_repository.dart';
import 'package:empresas_app/modules/company/usecase/search_company_use_case.dart';
import 'package:empresas_app/modules/login/datasource/login_data_source.dart';
import 'package:empresas_app/modules/login/presentation/bloc/login_bloc.dart';
import 'package:empresas_app/modules/login/presentation/page/login_screen.dart';
import 'package:empresas_app/modules/login/repository/login_repository.dart';
import 'package:empresas_app/modules/login/usecase/sign_in.dart';
import 'package:empresas_app/shared/network/custom_interceptor.dart';
import 'package:empresas_app/shared/network/http_adapter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EmpresasApp extends StatelessWidget {
  const EmpresasApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<CustomInterceptor>(
            create: (_) => CustomInterceptor()),
        RepositoryProvider<LoginRepository>(
          create: (context) => LoginRepository(
            datasource: LoginDataSource(
              HttpAdapter(
                baseUrl: environment.getVariable(variableName: 'EMPRESAS_API'),
                interceptor: context.read<CustomInterceptor>(),
              ),
            ),
          ),
        ),
        RepositoryProvider(
          create: (context) => CompanySearchRepository(
            dataSource: CompanyDataSource(
              httpAdapter: HttpAdapter(
                baseUrl: environment.getVariable(variableName: 'EMPRESAS_API'),
                interceptor: context.read<CustomInterceptor>(),
              ),
            ),
          ),
        ),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<LoginBloc>(create: (context) {
            final loginRepository = context.read<LoginRepository>();
            return LoginBloc(signIn: SignIn(repository: loginRepository));
          }),
          BlocProvider(create: (context) {
            final companySearchRepository =
                context.read<CompanySearchRepository>();
            return CompanySearchBloc(
                useCase:
                    SearchCompanyUseCase(repository: companySearchRepository));
          }),
        ],
        child: MaterialApp(
          title: 'Empresas',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            fontFamily: 'Roboto',
            primarySwatch: Colors.deepPurple,
            scaffoldBackgroundColor: Colors.white,
          ),
          home: const LoginScreen(),
        ),
      ),
    );
  }
}
