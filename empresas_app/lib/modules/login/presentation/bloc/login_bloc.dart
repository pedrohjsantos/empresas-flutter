import 'package:empresas_app/modules/login/presentation/bloc/login_event.dart';
import 'package:empresas_app/modules/login/presentation/bloc/login_state.dart';
import 'package:empresas_app/modules/login/usecase/sign_in.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final SignIn signIn;

  LoginBloc({required this.signIn}) : super(LoginInitialState()) {
    on<LoginSignInEvent>(_signIn);
  }

  Future _signIn(
    LoginSignInEvent event,
    Emitter<LoginState> emit,
  ) async {
    emit(LoginLoadingState());

    final user = await signIn.call(
      email: event.email,
      password: event.password,
    );

    user.fold(
      (exception) => emit(LoginErrorState()),
      (result) => emit(LoginSuccessState(user: result)),
    );
  }
}
