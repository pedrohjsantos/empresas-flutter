import 'package:empresas_app/shared/utils/color_style.dart';
import 'package:flutter/material.dart';

class EmpresasButton extends StatelessWidget {
  final Function? onPressed;
  final String text;
  final double? width;
  final Color? color;
  final double? radius;

  const EmpresasButton({
    required this.onPressed,
    required this.text,
    this.width,
    this.color,
    this.radius,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed != null ? () => onPressed!() : null,
      style: ElevatedButton.styleFrom(
        primary: color ?? ColorStyle.primary,
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 24),
        fixedSize: Size.fromWidth(width ?? double.infinity),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(radius ?? 50)),
        textStyle: const TextStyle(
          color: Colors.white,
          fontSize: 14,
          fontWeight: FontWeight.w500,
        ),
      ),
      child: Text(text),
    );
  }
}
