import 'package:empresas_app/modules/company/model/company_dto.dart';
import 'package:empresas_app/shared/component/empresas_app_bar.dart';
import 'package:empresas_app/shared/utils/color_style.dart';
import 'package:flutter/material.dart';

class CompanyDetailScreen extends StatelessWidget {
  final CompanyDTO company;
  const CompanyDetailScreen({required this.company, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: EmpresasAppBar(
        backgroundColor: ColorStyle.secundary,
        elevation: 1,
        showLeading: true,
        title: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              company.name,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 4),
            Text(
              company.companyType?.name ?? '',
              style: const TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
          ],
        ),
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 32),
        child: Text(
          company.description,
          style: TextStyle(
            color: ColorStyle.greyLight,
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}
