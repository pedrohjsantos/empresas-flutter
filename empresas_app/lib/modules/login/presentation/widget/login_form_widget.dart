import 'package:empresas_app/shared/component/empresas_button.dart';
import 'package:empresas_app/shared/component/empresas_text_form_field.dart';
import 'package:empresas_app/shared/utils/color_style.dart';
import 'package:empresas_app/shared/utils/extension/extension.dart';
import 'package:flutter/material.dart';

class LoginFormWidget extends StatefulWidget {
  final Function(String email, String password) onSubmit;
  final bool invalidCredentials;

  const LoginFormWidget({
    required this.onSubmit,
    required this.invalidCredentials,
    Key? key,
  }) : super(key: key);

  @override
  State<LoginFormWidget> createState() => _LoginFormWidgetState();
}

class _LoginFormWidgetState extends State<LoginFormWidget> {
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  bool isSubmitEnable = false;
  bool showPassword = false;

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.fromLTRB(16, 24, 16, 40),
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Digite seus dados para continuar.',
            style: TextStyle(
              color: ColorStyle.primary,
              fontSize: 14,
              fontWeight: FontWeight.w600,
            ),
          ),
          const SizedBox(height: 24),
          Form(
            key: _formKey,
            child: Column(
              children: [
                EmpresasTextFormField(
                  controller: _emailController,
                  keyboardType: TextInputType.emailAddress,
                  textInputAction: TextInputAction.next,
                  label: 'Email',
                  onChanged: (value) {
                    if (value.isEmpty || value.length == 1) {
                      _verifySubmit();
                    }
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Campo obrigatório';
                    }

                    if (!value.isValidEmail()) {
                      return 'Endereço de email inválido';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 20),
                EmpresasTextFormField(
                  controller: _passwordController,
                  label: 'Senha',
                  obscureText: !showPassword,
                  sufixIcon: IconButton(
                    icon: showPassword
                        ? Icon(Icons.visibility_off_outlined,
                            color: ColorStyle.greyLight)
                        : Icon(Icons.visibility_outlined,
                            color: ColorStyle.greyLight),
                    padding: const EdgeInsets.all(0),
                    onPressed: () {
                      setState(() {
                        showPassword = !showPassword;
                      });
                    },
                  ),
                  onChanged: (value) {
                    if (value.isEmpty || value.length == 1) {
                      _verifySubmit();
                    }
                  },
                  onFieldSubmitted: (value) {
                    _submitForm();
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Campo obrigatório';
                    }
                    return null;
                  },
                ),
                widget.invalidCredentials
                    ? Column(
                        children: [
                          const SizedBox(height: 4),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(
                                'Credenciais inválidas',
                                style: TextStyle(
                                  color: ColorStyle.inputTextError,
                                  fontSize: 14,
                                ),
                              ),
                              const SizedBox(width: 4),
                              Icon(
                                Icons.error_outline,
                                color: ColorStyle.inputTextError,
                              ),
                            ],
                          ),
                        ],
                      )
                    : const SizedBox.shrink(),
                const SizedBox(height: 40),
                EmpresasButton(
                  onPressed: isSubmitEnable ? _submitForm : null,
                  text: 'ENTRAR',
                  width: double.maxFinite,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _verifySubmit() {
    if (_emailController.text.isNotEmpty &&
        _passwordController.text.isNotEmpty) {
      setState(() {
        isSubmitEnable = true;
      });
    } else {
      setState(() {
        isSubmitEnable = false;
      });
    }
  }

  void _submitForm() {
    if (_formKey.currentState!.validate()) {
      widget.onSubmit(
        _emailController.text,
        _passwordController.text,
      );
    }
  }
}
