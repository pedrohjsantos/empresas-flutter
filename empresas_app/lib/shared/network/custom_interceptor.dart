import 'package:dio/dio.dart';

class CustomInterceptor extends Interceptor {
  late String? _accessToken = '';
  late String? _client = '';
  late String? _uid = '';

  CustomInterceptor();

  void setHeadersCredentials({
    required String accessToken,
    required String client,
    required String uid,
  }) {
    _accessToken = accessToken;
    _client = client;
    _uid = uid;
  }

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    options.headers.addAll({
      'access-token': _accessToken,
      'client': _client,
      'uid': _uid,
    });
    super.onRequest(options, handler);
  }
}
