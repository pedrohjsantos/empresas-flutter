import 'package:empresas_app/modules/login/model/user_dto.dart';

class TestUtilHelper {
  static Map<String, dynamic> userJson = {
    "investor": {
      "id": 1,
      "investor_name": "Teste Apple",
      "email": "testeapple@ioasys.com.br",
      "city": "BH",
      "country": "Brasil",
    },
  };

  static Map<String, List<String>> userHeaderJson = {
    "access-token": [''],
    "client": [''],
    "uid": [''],
  };

  static Map<String, dynamic> userFullJson = {
    "investor": {
      "id": 1,
      "investor_name": "Teste Apple",
      "email": "testeapple@ioasys.com.br",
      "city": "BH",
      "country": "Brasil",
    },
    "access-token": [''],
    "client": [''],
    "uid": [''],
  };

  static UserDTO user = UserDTO.fromJson(userFullJson);
}
