import 'package:empresas_app/modules/company/presentation/page/company_search_screen.dart';
import 'package:empresas_app/modules/login/presentation/bloc/login_bloc.dart';
import 'package:empresas_app/modules/login/presentation/bloc/login_event.dart';
import 'package:empresas_app/modules/login/presentation/bloc/login_state.dart';
import 'package:empresas_app/modules/login/presentation/widget/login_form_widget.dart';
import 'package:empresas_app/modules/login/presentation/widget/welcome_widget.dart';
import 'package:empresas_app/shared/network/custom_interceptor.dart';
import 'package:empresas_app/shared/utils/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool invalidCredentials = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return BlocConsumer<LoginBloc, LoginState>(
      builder: (context, state) {
        if (state is LoginLoadingState || state is LoginSuccessState) {
          return const Center(child: CupertinoActivityIndicator(radius: 20));
        }

        if (state is LoginErrorState) {
          invalidCredentials = true;
        }

        return _buildLoginContent();
      },
      listener: (context, state) {
        if (state is LoginSuccessState) {
          context.read<CustomInterceptor>().setHeadersCredentials(
                accessToken: state.user.accessToken,
                client: state.user.client,
                uid: state.user.uid,
              );
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (_) => const CompanySearchScreen()));
        }
      },
    );
  }

  Widget _buildLoginContent() {
    return Stack(
      children: [
        Positioned(
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
          child: Image.asset(
            'assets/images/login_background.png',
            fit: BoxFit.fill,
          ),
        ),
        Positioned(
          top: 100,
          right: 0,
          child: Image.asset(
            'assets/images/2x/logo_background.png',
            scale: 2,
          ),
        ),
        Positioned(
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: [
                  ColorStyle.gradientDark.withOpacity(0.5),
                  ColorStyle.gradientDark.withOpacity(0),
                ],
              ),
            ),
          ),
        ),
        Positioned(
          left: 0,
          bottom: 0,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const WelcomeWidget(),
              const SizedBox(height: 30),
              LoginFormWidget(
                invalidCredentials: invalidCredentials,
                onSubmit: (email, password) {
                  context
                      .read<LoginBloc>()
                      .add(LoginSignInEvent(email: email, password: password));
                },
              ),
            ],
          ),
        ),
      ],
    );
  }
}
