import 'package:dio/dio.dart';
import 'package:empresas_app/shared/network/custom_interceptor.dart';

enum HttpMethod { get, post }

abstract class HttpAdapterInterface {
  Future<Response> request<T>({
    required HttpMethod method,
    required String path,
    dynamic data,
    Map<String, dynamic>? params,
    Map<String, dynamic>? headers,
  });
}

class HttpAdapter implements HttpAdapterInterface {
  final Dio _clientHttp;
  final bool showLog;
  final String baseUrl;
  final Map<String, dynamic>? baseHeaders;
  final CustomInterceptor? interceptor;

  HttpAdapter({
    required this.baseUrl,
    this.baseHeaders,
    this.showLog = false,
    this.interceptor,
  }) : _clientHttp = Dio(
          BaseOptions(
            baseUrl: baseUrl,
            headers: baseHeaders,
          ),
        )..interceptors.add(
            interceptor!,
          );

  @override
  Future<Response> request<T>({
    required HttpMethod method,
    required String path,
    dynamic data,
    Map<String, dynamic>? params,
    Map<String, dynamic>? headers,
  }) async {
    try {
      String apiMethod = '';

      switch (method) {
        case HttpMethod.get:
          apiMethod = 'GET';
          break;
        case HttpMethod.post:
          apiMethod = 'POST';
          break;
        default:
      }

      final reponse = await _clientHttp.request(
        path,
        data: data,
        queryParameters: params,
        options: Options(
          method: apiMethod,
          headers: headers,
        ),
      );

      return reponse;
    } on Exception catch (error) {
      return Future.error(error);
    }
  }
}
