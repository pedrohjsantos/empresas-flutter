abstract class LoginEvent {}

class LoginSignInEvent extends LoginEvent {
  final String email;
  final String password;

  LoginSignInEvent({required this.email, required this.password});
}
