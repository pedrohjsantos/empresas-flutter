import 'package:dio/dio.dart';
import 'package:empresas_app/modules/login/datasource/login_data_source.dart';
import 'package:empresas_app/shared/network/http_adapter.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../test_util_helper.dart';

class HttpAdapterMock extends Mock implements HttpAdapterInterface {}

void main() {
  final _httpAdapter = HttpAdapterMock();
  final _dataSource = LoginDataSource(_httpAdapter);
  final responseData = TestUtilHelper.userJson;
  final responseHeader = TestUtilHelper.userHeaderJson;

  when(() => _httpAdapter.request(
        method: HttpMethod.post,
        path: '/users/auth/sign_in',
        params: {
          'email': '',
          'password': '',
        },
      )).thenAnswer(
    (_) async => Response(
      data: responseData,
      headers: Headers.fromMap(responseHeader),
      statusCode: 200,
      requestOptions: RequestOptions(path: '/users/auth/sign_in'),
    ),
  );

  test(
    'Should fetch a user',
    () async {
      final response = await _dataSource.signIn(email: '', password: '');

      expect(response.isRight(), equals(true));
    },
  );
}
