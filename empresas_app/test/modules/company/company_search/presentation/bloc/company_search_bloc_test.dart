import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:empresas_app/modules/company/model/company_dto.dart';
import 'package:empresas_app/modules/company/presentation/bloc/company_search_bloc.dart';
import 'package:empresas_app/modules/company/presentation/bloc/company_search_event.dart';
import 'package:empresas_app/modules/company/presentation/bloc/company_search_state.dart';
import 'package:empresas_app/modules/company/repository/company_search_repository_inteface.dart';
import 'package:empresas_app/modules/company/usecase/search_company_use_case.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../test_util_helper.dart';

class CompanySearchRepositoryMock extends Mock
    implements CompanySearchRepositoryInterface {}

void main() {
  final repository = CompanySearchRepositoryMock();
  final searchCompany = SearchCompanyUseCase(repository: repository);
  final companies = TestUtilHelper.companies;
  final companiesEmpty = TestUtilHelper.companiesEmpty;
  final errorEither =
      Left<dynamic, List<CompanyDTO>>(Exception('Exception test'));
  final companiesEither = Right<dynamic, List<CompanyDTO>>(companies);
  final companiesEmptyEither = Right<dynamic, List<CompanyDTO>>(companiesEmpty);

  group(
    'Company search bloc tests |',
    () {
      blocTest<CompanySearchBloc, CompanySearchState>(
        'Check initial state',
        build: () => CompanySearchBloc(useCase: searchCompany),
        verify: (bloc) => expect(bloc.state, isA<CompanySearchInitalState>()),
      );

      blocTest<CompanySearchBloc, CompanySearchState>(
        'Check loaded state',
        build: () => CompanySearchBloc(useCase: searchCompany),
        act: (bloc) =>
            bloc.add(CompanySearchCompanyEvent(searchString: 'teste')),
        setUp: () {
          when(() => repository.searchCompany(searchString: 'teste'))
              .thenAnswer((_) => SynchronousFuture(companiesEither));
        },
        expect: () => [
          isA<CompanySearchLoadingState>(),
          isA<CompanySearchLoadedState>(),
        ],
      );

      blocTest<CompanySearchBloc, CompanySearchState>(
        'Check empty state',
        build: () => CompanySearchBloc(useCase: searchCompany),
        act: (bloc) =>
            bloc.add(CompanySearchCompanyEvent(searchString: 'teste')),
        setUp: () {
          when(() => repository.searchCompany(searchString: 'teste'))
              .thenAnswer((_) => SynchronousFuture(companiesEmptyEither));
        },
        expect: () => [
          isA<CompanySearchLoadingState>(),
          isA<CompanySearchEmptyState>(),
        ],
      );

      blocTest<CompanySearchBloc, CompanySearchState>(
        'Check error state',
        build: () => CompanySearchBloc(useCase: searchCompany),
        act: (bloc) =>
            bloc.add(CompanySearchCompanyEvent(searchString: 'teste')),
        setUp: () {
          when(() => repository.searchCompany(searchString: 'teste'))
              .thenAnswer((_) => SynchronousFuture(errorEither));
        },
        expect: () => [
          isA<CompanySearchLoadingState>(),
          isA<CompanySearchErrorState>(),
        ],
      );
    },
  );
}
