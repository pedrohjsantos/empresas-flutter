import 'package:dio/dio.dart';
import 'package:empresas_app/modules/company/datasource/company_data_source.dart';
import 'package:empresas_app/shared/network/http_adapter.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../test_util_helper.dart';

class HttpAdapterMock extends Mock implements HttpAdapterInterface {}

void main() {
  final _httpAdapter = HttpAdapterMock();
  final _dataSource = CompanyDataSource(httpAdapter: _httpAdapter);
  final responseData = TestUtilHelper.companiesJson;

  when(() => _httpAdapter.request(
        method: HttpMethod.get,
        path: '/enterprises?name=teste',
      )).thenAnswer(
    (_) async => Response(
      data: responseData,
      statusCode: 200,
      requestOptions: RequestOptions(path: '/enterprises?name=teste'),
    ),
  );

  test(
    'Should fetch some companies',
    () async {
      final response = await _dataSource.companySearch(searchString: 'teste');

      expect(response.isRight(), true);
    },
  );
}
