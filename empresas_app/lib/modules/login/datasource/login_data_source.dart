import 'package:dartz/dartz.dart';
import 'package:empresas_app/modules/login/model/user_dto.dart';
import 'package:empresas_app/shared/network/http_adapter.dart';

abstract class LoginDataSourceInterface {
  Future<Either<dynamic, UserDTO>> signIn(
      {required String email, required String password});
}

class LoginDataSource extends LoginDataSourceInterface {
  final HttpAdapterInterface _httpAdapter;

  LoginDataSource(this._httpAdapter);

  @override
  Future<Either<dynamic, UserDTO>> signIn({
    required String email,
    required String password,
  }) async {
    try {
      final response = await _httpAdapter.request(
        method: HttpMethod.post,
        path: '/users/auth/sign_in',
        params: {
          'email': email,
          'password': password,
        },
      );

      Map<String, dynamic> json = response.data;
      Map<String, dynamic> credentials = {
        'access-token': response.headers['access-token'],
        'client': response.headers['client'],
        'uid': response.headers['uid'],
      };

      json.addAll(credentials);

      return Right(UserDTO.fromJson(json));
    } catch (e) {
      return Left(e);
    }
  }
}
