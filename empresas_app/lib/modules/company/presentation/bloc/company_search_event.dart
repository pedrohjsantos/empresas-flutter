abstract class CompanySearchEvent {}

class CompanySearchCompanyEvent extends CompanySearchEvent {
  final String searchString;

  CompanySearchCompanyEvent({required this.searchString});
}
