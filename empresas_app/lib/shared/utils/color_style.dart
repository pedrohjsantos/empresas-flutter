import 'package:flutter/material.dart';

class ColorStyle {
  static Color primary = _getColorByHex('#271019');
  static Color secundary = _getColorByHex('#7E0D30');

  static Color greyLight = _getColorByHex('#939393');

  static Color inputText = _getColorByHex('#1A1B1C');
  static Color inputTextError = _getColorByHex('#BD4E4E');
  static Color inputBorder = _getColorByHex('#C4C4C4');
  static Color inputBorderError = _getColorByHex('#FFA9A9');
  static Color inputBorderFocused = _getColorByHex('#994BBD');

  static Color buttonDisabled = _getColorByHex('#8A8A8A');

  static Color gradientDark = _getColorByHex('#111111');

  static Color _getColorByHex(String hex) =>
      Color(int.parse('0xFF' + hex.replaceAll('#', '')));
}
