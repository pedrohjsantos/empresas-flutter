import 'package:dartz/dartz.dart';
import 'package:empresas_app/modules/login/model/user_dto.dart';

abstract class LoginRepositoryInterface {
  Future<Either<dynamic, UserDTO>> signIn(
      {required String email, required String password});
}
