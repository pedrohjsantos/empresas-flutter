import 'package:dartz/dartz.dart';
import 'package:empresas_app/modules/login/model/user_dto.dart';
import 'package:empresas_app/modules/login/repository/login_repository_interface.dart';

class SignIn {
  final LoginRepositoryInterface repository;

  SignIn({required this.repository});

  Future<Either<dynamic, UserDTO>> call(
          {required String email, required String password}) =>
      repository.signIn(email: email, password: password);
}
