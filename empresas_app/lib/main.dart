import 'package:empresas_app/app/empresas_app.dart';
import 'package:empresas_app/app/environment.dart';
import 'package:flutter/material.dart';

void main() async {
  await environment.loadEvironment();

  runApp(const EmpresasApp());
}
