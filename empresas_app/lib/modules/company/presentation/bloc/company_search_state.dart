import 'package:empresas_app/modules/company/model/company_dto.dart';

abstract class CompanySearchState {}

class CompanySearchInitalState extends CompanySearchState {}

class CompanySearchLoadingState extends CompanySearchState {}

class CompanySearchLoadedState extends CompanySearchState {
  final List<CompanyDTO> companies;

  CompanySearchLoadedState({required this.companies});
}

class CompanySearchEmptyState extends CompanySearchState {}

class CompanySearchErrorState extends CompanySearchState {}
