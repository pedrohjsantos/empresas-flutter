import 'package:dartz/dartz.dart';
import 'package:empresas_app/modules/company/model/company_dto.dart';

abstract class CompanySearchRepositoryInterface {
  Future<Either<dynamic, List<CompanyDTO>>> searchCompany({
    required String searchString,
  });
}
