import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class EmpresasAppBar extends StatefulWidget implements PreferredSizeWidget {
  final Widget title;
  final bool? showLeading;
  final Function? leadingAction;
  final Color? backgroundColor;
  final double? elevation;

  const EmpresasAppBar({
    required this.title,
    this.showLeading = false,
    this.leadingAction,
    this.backgroundColor,
    this.elevation = 1.0,
    Key? key,
  }) : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(120);

  @override
  State<EmpresasAppBar> createState() => _EmpresasAppBarState();
}

class _EmpresasAppBarState extends State<EmpresasAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: widget.backgroundColor ?? Colors.white,
      elevation: widget.elevation,
      automaticallyImplyLeading: false,
      flexibleSpace: Padding(
        padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
        child: Align(
          alignment: Alignment.bottomCenter,
          child: Stack(
            children: [
              widget.showLeading!
                  ? Positioned(
                      bottom: 0,
                      child: InkWell(
                        onTap: () {
                          if (widget.leadingAction != null) {
                            widget.leadingAction!();
                          } else {
                            Navigator.of(context).pop();
                          }
                        },
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                        child: SvgPicture.asset(
                          'assets/images/icon/arrow_back.svg',
                        ),
                      ),
                    )
                  : const SizedBox.shrink(),
              widget.showLeading!
                  ? Positioned.fill(
                      bottom: 0,
                      child: widget.title,
                    )
                  : widget.title,
            ],
          ),
        ),
      ),
    );
  }
}
