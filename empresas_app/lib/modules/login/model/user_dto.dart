class UserDTO {
  late int id;
  late String name;
  late String email;
  late String city;
  late String country;
  late String _accessToken;
  late String _client;
  late String _uid;

  UserDTO.fromJson(Map<String, dynamic> json) {
    id = json['investor']['id'];
    name = json['investor']['investor_name'];
    email = json['investor']['email'];
    city = json['investor']['city'];
    country = json['investor']['country'];

    List listAux = json['access-token'] as List;
    _accessToken = listAux.first;

    listAux = json['client'] as List;
    _client = listAux.first;

    listAux = json['uid'] as List;
    _uid = listAux.first;
  }

  String get accessToken => _accessToken;
  String get client => _client;
  String get uid => _uid;
}
