import 'package:empresas_app/shared/utils/color_style.dart';
import 'package:flutter/material.dart';

class EmpresasTextFormField extends StatelessWidget {
  final TextEditingController controller;
  final FocusNode? focusNode;
  final String? Function(String?)? validator;
  final Function(String)? onChanged;
  final Function(String)? onFieldSubmitted;
  final String? label;
  final Widget? prefixIcon;
  final Widget? sufixIcon;
  final bool obscureText;
  final TextInputType? keyboardType;
  final TextInputAction? textInputAction;

  const EmpresasTextFormField({
    required this.controller,
    this.focusNode,
    this.validator,
    this.onChanged,
    this.onFieldSubmitted,
    this.label,
    this.prefixIcon,
    this.sufixIcon,
    this.obscureText = false,
    this.keyboardType,
    this.textInputAction,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      focusNode: focusNode,
      keyboardType: keyboardType,
      obscureText: obscureText,
      validator: validator,
      onChanged: onChanged,
      onFieldSubmitted: onFieldSubmitted,
      textInputAction: textInputAction,
      decoration: InputDecoration(
        labelText: label,
        prefixIcon: Padding(
          padding: const EdgeInsets.all(8.0),
          child: prefixIcon,
        ),
        prefixIconConstraints:
            const BoxConstraints(maxHeight: 40, maxWidth: 40),
        suffixIcon: Padding(
          padding: const EdgeInsets.only(right: 16.0),
          child: sufixIcon,
        ),
        suffixIconConstraints:
            const BoxConstraints(maxHeight: 40, maxWidth: 40),
        border: OutlineInputBorder(
          borderSide: BorderSide(color: ColorStyle.inputBorder),
          borderRadius: BorderRadius.circular(8),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: ColorStyle.inputBorderError),
          borderRadius: BorderRadius.circular(8),
        ),
        errorStyle: TextStyle(
          color: ColorStyle.inputTextError,
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: ColorStyle.inputBorderFocused),
          borderRadius: BorderRadius.circular(8),
        ),
      ),
    );
  }
}
