import 'package:dartz/dartz.dart';
import 'package:empresas_app/modules/company/model/company_dto.dart';
import 'package:empresas_app/shared/network/http_adapter.dart';

abstract class CompanyDataSourceInterface {
  Future<Either<dynamic, List<CompanyDTO>>> companySearch(
      {required String searchString});
}

class CompanyDataSource extends CompanyDataSourceInterface {
  final HttpAdapterInterface httpAdapter;

  CompanyDataSource({required this.httpAdapter});

  @override
  Future<Either<dynamic, List<CompanyDTO>>> companySearch(
      {required String searchString}) async {
    try {
      final response = await httpAdapter.request(
        method: HttpMethod.get,
        path: '/enterprises?name=$searchString',
      );

      Map<String, dynamic> json = response.data;
      List companies = json['enterprises'] as List;
      return Right(companies.map((e) => CompanyDTO.fromJson(e)).toList());
    } catch (e) {
      return Left(e);
    }
  }
}
