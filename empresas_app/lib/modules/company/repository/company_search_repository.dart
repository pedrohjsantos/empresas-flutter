import 'package:dartz/dartz.dart';
import 'package:empresas_app/modules/company/datasource/company_data_source.dart';
import 'package:empresas_app/modules/company/model/company_dto.dart';
import 'package:empresas_app/modules/company/repository/company_search_repository_inteface.dart';

class CompanySearchRepository extends CompanySearchRepositoryInterface {
  final CompanyDataSourceInterface dataSource;

  CompanySearchRepository({required this.dataSource});

  @override
  Future<Either<dynamic, List<CompanyDTO>>> searchCompany({
    required String searchString,
  }) =>
      dataSource.companySearch(searchString: searchString);
}
