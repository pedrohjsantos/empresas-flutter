import 'package:dartz/dartz.dart';
import 'package:empresas_app/modules/company/model/company_dto.dart';
import 'package:empresas_app/modules/company/repository/company_search_repository_inteface.dart';

class SearchCompanyUseCase {
  final CompanySearchRepositoryInterface repository;

  SearchCompanyUseCase({required this.repository});

  Future<Either<dynamic, List<CompanyDTO>>> call(
          {required String searchString}) =>
      repository.searchCompany(searchString: searchString);
}
