import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:empresas_app/modules/login/model/user_dto.dart';
import 'package:empresas_app/modules/login/presentation/bloc/login_bloc.dart';
import 'package:empresas_app/modules/login/presentation/bloc/login_event.dart';
import 'package:empresas_app/modules/login/presentation/bloc/login_state.dart';
import 'package:empresas_app/modules/login/repository/login_repository_interface.dart';
import 'package:empresas_app/modules/login/usecase/sign_in.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../test_util_helper.dart';

class LoginRepositoryMock extends Mock implements LoginRepositoryInterface {}

void main() {
  final repository = LoginRepositoryMock();
  final signIn = SignIn(repository: repository);
  final user = TestUtilHelper.user;
  final errorEither = Left<dynamic, UserDTO>(Exception('Exception test'));
  final userEither = Right<dynamic, UserDTO>(user);

  group(
    'Login bloc tests |',
    () {
      blocTest<LoginBloc, LoginState>(
        'Check initial state',
        build: () => LoginBloc(signIn: signIn),
        verify: (bloc) => expect(bloc.state, isA<LoginInitialState>()),
      );

      blocTest<LoginBloc, LoginState>(
        'Check success state',
        build: () => LoginBloc(signIn: signIn),
        act: (bloc) => bloc.add(LoginSignInEvent(email: '', password: '')),
        setUp: () {
          when(() => repository.signIn(email: '', password: ''))
              .thenAnswer((_) => SynchronousFuture(userEither));
        },
        expect: () => [
          isA<LoginLoadingState>(),
          isA<LoginSuccessState>(),
        ],
      );

      blocTest<LoginBloc, LoginState>(
        'Check error state',
        build: () => LoginBloc(signIn: signIn),
        act: (bloc) => bloc.add(LoginSignInEvent(email: '', password: '')),
        setUp: () {
          when(() => repository.signIn(email: '', password: ''))
              .thenAnswer((_) => SynchronousFuture(errorEither));
        },
        expect: () => [
          isA<LoginLoadingState>(),
          isA<LoginErrorState>(),
        ],
      );
    },
  );
}
