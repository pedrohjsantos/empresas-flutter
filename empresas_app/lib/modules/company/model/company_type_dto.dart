class CompanyTypeDTO {
  late int id;
  late String name;

  CompanyTypeDTO.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['enterprise_type_name'];
  }
}
