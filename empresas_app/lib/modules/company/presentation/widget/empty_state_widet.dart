import 'package:empresas_app/shared/utils/color_style.dart';
import 'package:flutter/material.dart';

class EmptyStateWidget extends StatelessWidget {
  const EmptyStateWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const SizedBox(height: 40),
        Image.asset(
          'assets/images/2x/search_empty.png',
          scale: 2,
        ),
        const SizedBox(height: 16),
        Text(
          'Empresa não encontrada',
          style: TextStyle(
            color: ColorStyle.greyLight,
            fontSize: 16,
          ),
        ),
      ],
    );
  }
}
