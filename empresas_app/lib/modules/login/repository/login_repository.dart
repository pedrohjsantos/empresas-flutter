import 'package:dartz/dartz.dart';
import 'package:empresas_app/modules/login/datasource/login_data_source.dart';
import 'package:empresas_app/modules/login/model/user_dto.dart';
import 'package:empresas_app/modules/login/repository/login_repository_interface.dart';

class LoginRepository extends LoginRepositoryInterface {
  final LoginDataSourceInterface datasource;

  LoginRepository({required this.datasource});

  @override
  Future<Either<dynamic, UserDTO>> signIn({
    required String email,
    required String password,
  }) =>
      datasource.signIn(email: email, password: password);
}
