import 'package:flutter_dotenv/flutter_dotenv.dart';

EmpresasEnvironment environment = Environment();

abstract class EmpresasEnvironment {
  EmpresasEnvironment();
  Future<void> loadEvironment();

  String getVariable({required String variableName});
}

class Environment implements EmpresasEnvironment {
  @override
  Future<void> loadEvironment() async {
    const fileEnv = 'assets/environment/.env';
    await dotenv.load(fileName: fileEnv);
  }

  @override
  String getVariable({required String variableName}) {
    String variable = dotenv.get(variableName);
    return variable;
  }
}
