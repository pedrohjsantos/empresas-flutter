import 'package:empresas_app/modules/company/presentation/bloc/company_search_bloc.dart';
import 'package:empresas_app/modules/company/presentation/bloc/company_search_event.dart';
import 'package:empresas_app/modules/company/presentation/bloc/company_search_state.dart';
import 'package:empresas_app/modules/company/presentation/page/company_detail_screen.dart';
import 'package:empresas_app/modules/company/presentation/widget/companies_grid_view.dart';
import 'package:empresas_app/modules/company/presentation/widget/empty_state_widet.dart';
import 'package:empresas_app/shared/component/empresas_app_bar.dart';
import 'package:empresas_app/shared/component/empresas_text_form_field.dart';
import 'package:empresas_app/shared/utils/color_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

class CompanySearchScreen extends StatefulWidget {
  const CompanySearchScreen({Key? key}) : super(key: key);

  @override
  State<CompanySearchScreen> createState() => _CompanySearchScreenState();
}

class _CompanySearchScreenState extends State<CompanySearchScreen> {
  final _searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: EmpresasAppBar(
        title: Text(
          'Pesquise por uma empresa',
          maxLines: 2,
          style: TextStyle(
            color: ColorStyle.primary,
            fontSize: 36,
            fontWeight: FontWeight.bold,
          ),
        ),
        elevation: 0,
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: ListView(
        children: [
          _buildSearch(),
          const SizedBox(height: 16),
          BlocBuilder<CompanySearchBloc, CompanySearchState>(
            builder: (context, state) {
              if (state is CompanySearchLoadingState) {
                return Center(
                  child: Container(
                    margin: const EdgeInsets.only(top: 80),
                    child: const CupertinoActivityIndicator(radius: 20),
                  ),
                );
              }

              if (state is CompanySearchErrorState) {
                return const SizedBox.shrink();
              }

              if (state is CompanySearchInitalState) {
                return const SizedBox.shrink();
              }

              if (state is CompanySearchEmptyState) {
                return const EmptyStateWidget();
              }

              state = state as CompanySearchLoadedState;

              return CompaniesGridView(
                companies: state.companies,
                onClick: (company) {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (_) => CompanyDetailScreen(company: company)));
                },
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _buildSearch() {
    return EmpresasTextFormField(
      controller: _searchController,
      label: 'Buscar...',
      prefixIcon: SvgPicture.asset(
        'assets/images/icon/search.svg',
      ),
      onChanged: (value) {
        context
            .read<CompanySearchBloc>()
            .add(CompanySearchCompanyEvent(searchString: value));
      },
    );
  }
}
