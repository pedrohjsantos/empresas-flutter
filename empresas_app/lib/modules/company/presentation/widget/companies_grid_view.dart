import 'package:empresas_app/modules/company/model/company_dto.dart';
import 'package:empresas_app/shared/utils/color_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class CompaniesGridView extends StatelessWidget {
  final List<CompanyDTO> companies;
  final Function(CompanyDTO) onClick;

  const CompaniesGridView(
      {required this.companies, required this.onClick, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StaggeredGrid.count(
      crossAxisCount: 2,
      mainAxisSpacing: 16,
      crossAxisSpacing: 16,
      children: companies.map((e) => _companyCard(company: e)).toList(),
    );
  }

  Widget _companyCard({required CompanyDTO company}) {
    return Center(
      child: InkWell(
        onTap: () => onClick(company),
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        child: Stack(
          children: [
            Container(
              height: 108,
              width: 152,
              decoration: BoxDecoration(
                color: ColorStyle.secundary,
                borderRadius: BorderRadius.circular(16),
              ),
            ),
            Positioned(
              bottom: 0,
              child: Container(
                padding: const EdgeInsets.symmetric(vertical: 8),
                width: 152,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius:
                      const BorderRadius.vertical(bottom: Radius.circular(16)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.4),
                      blurRadius: 2,
                      offset: const Offset(2, 5),
                    ),
                  ],
                ),
                child: Text(
                  company.name,
                  textAlign: TextAlign.center,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: ColorStyle.greyLight,
                    fontSize: 14,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
